/**
 * Main JS for vAuto code challenge
 *
 * @author Chris Sealy
 */

/*
 *  1. Each word in the input string is replaced with the following: the first letter of the word,
 *     the count of distinct letters between the first and last letter, and the last letter of the word.
 *     For example, "Automotive" would be replaced by "A6e".
 *  2. A "word" is defined as a sequence of alphabetic characters, delimited by any non-alphabetic characters.
 *  3. Any non-alphabetic character in the input string should appear in the output string in its original
 *     relative location.
 * */
(function () {
    'use strict';
    //function to filter and convert input string according to rules of the challenge
    document.getElementById('btn-convert').onclick = function() {
        return textAnalysis();
    };

    /**
     * textAnalysis - Main function that will take the user input in the textarea and run translation on it and
     * populate the result area of the markup with the translated string.
     * @param none
     */
    function textAnalysis() {
        var userStr = document.getElementById("testbed").value;
        var result = document.getElementById("result");
        var nonAlpha = /[\W_0-9]+/g;
        var validTest = (userStr.match(/[a-z]/ig) || []).length; //test if input string even has alphabetic characters
        if (userStr.length > 0 && validTest > 0) {
             //split words by checking for any non-alphanumeric entries in the string
            var strArray = userStr.split(nonAlpha);
            //filter the new array and remove empty strings
            strArray = strArray.filter(Boolean);
            //convert each word in string array into criteria from exercise (first letter, the count of distinct
            // letters between the first and last letter, and the last letter)
            var wordArray = createConvertArray(strArray);

            //count var for the number of "spaces" between legit words
            var spacerArray = userStr.match(nonAlpha);
            var startSpace = userStr.search(nonAlpha);

            if(wordArray){
                result.innerHTML = genFinalString(wordArray, spacerArray, startSpace);
            }
        } else {
            //non valid string outputs error to page element
            result.innerHTML = "No result due to non alphabetic string.";
        }
    };

    /**
     * createConvertArray - translation function that takes in the user input string and process it into
     * an array containing translated words corresponding to the exercise criteria.
     * @param {String} strArray 
     * @return {Array} tmpArr
     */
    function createConvertArray(strArray) {
        var tmpWord, newWord, wordLen = 0, firstLtr, lastLtr, currLtr, countChars = 0;
        var letterCache = {}, tmpArr = [];
        var strLen = strArray.length;
        for (var i = 0; i < strLen; i++) {
            tmpWord = strArray[i];
            wordLen = tmpWord.length;
            firstLtr = tmpWord.charAt(0);
            lastLtr = tmpWord.charAt(wordLen - 1);
            letterCache = {};
            for (var j = 1; j < (wordLen - 1 ); j++) {
                currLtr = tmpWord.charAt(j);
                letterCache[currLtr] = (isNaN(letterCache[currLtr]) ? 1 : letterCache[currLtr] + 1);
            }
            countChars = Object.keys(letterCache).length;
            newWord = firstLtr + countChars + lastLtr;

            tmpArr.push(newWord);
        }
        if(tmpArr.length > 0){
            return tmpArr;
        }else{
            return false;
        }
    }

    /**
     * genFinalString - last portion of the translation is to take the new array of translated words,
     * and the array of space characters and combine them into a result string
     * @param {Array} words
     * @param {Array} spaces
     * @param {Boolean} spFlag 
     * @return {String} resultStr
     */
    function genFinalString(words, spaces, spFlag){
        if(spaces !== null && typeof spaces !== 'undefined'){
            var spacerLen = spaces.length;
        }else{
            var spacerLen = 0;
        }
        var resultStr = '', i = 0, j = 0;
        var wordLen = words.length;
        while (i < wordLen || j < spacerLen) {
            if (spFlag === 0) {
                if (j < spacerLen) {
                    resultStr += spaces[j++];
                }
                if (i < wordLen) {
                    resultStr += words[i++];
                }
            } else {
                if (i < wordLen) {
                    resultStr += words[i++];
                }
                if (j < spacerLen) {
                    resultStr += spaces[j++];
                }
            }
        }
        //output new result string to page element -- spaces to be replaced with
        //HTML friendly '&nbsp' for display purposes ONLY
        resultStr = resultStr.replace(/\s/g, "&nbsp;");

        return resultStr;
    }
})();
