# vAuto Code Challenge #

This was part of the interview process for vAuto to create a coded solution (language agnostic) to the challenge criteria.

The challenge has three criteria:

1. Each word in the input string is replaced with the following: the first letter of the word, the count of distinct letters between the first and last letter, and the last letter of the word. For example, "Automotive" would be replaced by "A6e".
2. A "word" is defined as a sequence of alphabetic characters, delimited by any non-alphabetic characters.
3. Any non-alphabetic character in the input string should appear in the output string in its original relative location.

The code presented was my solution to the challenge. It is written in pure Javascript with simple presentation markup in HTML/CSS.

I got points for accuracy and completeness of the solution.

While I didn't past this phase, I got some BS about maintainability and performance issues. I believe that's nonsense since the code is very concise JS and everything runs in three small functions.  I tested my code in Chrome 46 using over 119,000 words (290 pages or 745,900 characters) of generated random "Lorem Ipsum" text and got a result in under 560ms (measured in Chrome Developer Tools). My code performs as well as can be expected from JS in a browser environment. I think their engineers don't probably even have a command of the English language let alone any web languages. Oh I'm not SALTY! :-)

The code is concise and well documented. Use it for learning / educational purposes ONLY. 

Maybe it can help someone, that's all it is good for now.

### What is this repository for? ###

* Use for example of string manipulations in JavaScript

### How do I get set up? ###

* Clone or Download repo and run 'index.html' in a browser

### Who do I talk to? ###

* Repo owner or admin - Chris Sealy